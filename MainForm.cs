﻿namespace LanguageRegistration
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Globalization;
    using System.Linq;
    using System.Windows.Forms;
    using System.IO;
    using System.Runtime.Serialization.Formatters.Binary;

    public partial class MainForm : Form
    {
        List<CultureExport> ExportList;
        public MainForm()
        {
            this.InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            ExportList = new List<CultureExport>();
            this.RefreshList();
        }

        private void RefreshList()
        {
            this.culturesListBox.Items.Clear();
            List<CultureInfo> culturesList = new List<CultureInfo>(CultureInfo.GetCultures(CultureTypes.AllCultures));
            culturesList.Sort(new CultureComparer());
            foreach (CultureInfo ci in culturesList)
            {
                if (!string.IsNullOrEmpty(ci.Name))
                {
                    CultureListItem item = new CultureListItem();
                    item.DisplayName = ci.Name + " [" + ci.EnglishName + "]";
                    item.CultureName = ci.Name;
                    item.value = ci;
                    culturesListBox.Items.Add(item);
                }
            }
        }

        private void FillFormWithCultureInfo(CultureInfo ci)
        {
            nnTextBox.Text = ci.NativeName;
            enNameTextBox.Text = ci.EnglishName;
            nameTextBox.Text = ci.Name;
            ieftTextBox.Text = ci.IetfLanguageTag;
            isoNameTextBox.Text = ci.ThreeLetterISOLanguageName;
            winNameTextbox.Text = ci.ThreeLetterWindowsLanguageName;
            iso2TextBox.Text = ci.TwoLetterISOLanguageName;
            LCIDtextBox.Text = ci.LCID.ToString();
            try
            {
                RegionInfo regInfo = new RegionInfo(ci.Name);
                regNameTextBox.Text = regInfo.Name;
                regNativeNameTextBox.Text = regInfo.NativeName;
                engNameNameTextBox.Text = regInfo.EnglishName;
                currencyEnglishNameTextBox.Text = regInfo.CurrencyEnglishName;
                isoCurrSymbolTextBox.Text = regInfo.ISOCurrencySymbol;
                currNativeNameTextBox.Text = regInfo.CurrencyNativeName;
                curSymbolTextBox.Text = regInfo.CurrencySymbol;
            }
            catch (Exception)
            {
                regNameTextBox.Text = string.Empty;
                regNativeNameTextBox.Text = string.Empty;
                engNameNameTextBox.Text = string.Empty;
                currencyEnglishNameTextBox.Text = string.Empty;
                isoCurrSymbolTextBox.Text = string.Empty;
                currNativeNameTextBox.Text = string.Empty;
                curSymbolTextBox.Text = string.Empty;
            }
        }

        private void culturesListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            CultureListItem item = (CultureListItem)culturesListBox.SelectedItem;
            if (item == null)
            {
                return;
            }

            this.FillFormWithCultureInfo(new CultureInfo(item.CultureName));
        }

        private void btnUnRegister_Click(object sender, EventArgs e)
        {
            if (culturesListBox.SelectedIndex == -1)
            {
                MessageBox.Show("You should select a culture");
                return;
            }

            DialogResult result = MessageBox.Show("Are you sure you want to unregister selected culture?", "Confirmation", MessageBoxButtons.YesNoCancel);
            if (result == DialogResult.Yes)
            {
                CultureListItem item = (CultureListItem)culturesListBox.SelectedItem;
                CultureAndRegionInfoBuilder.Unregister(item.CultureName);
                this.RefreshList();
                item = (CultureListItem)culturesListBox.Items[0];
                this.FillFormWithCultureInfo(new CultureInfo(item.CultureName));
            }
        }

        private bool IsCultureExist(string cultureName)
        {
            foreach (CultureInfo ci in CultureInfo.GetCultures(CultureTypes.AllCultures))
            {
                if (ci.Name == cultureName)
                {
                    return true;
                }
            }

            return false;
        }

        private void regNameTextBox_Leave(object sender, EventArgs e)
        {
            TextBox textbox = (TextBox)sender;
            if (string.IsNullOrEmpty(textbox.Text))
            {
                MessageBox.Show("You can't leave the textbox blank");
            }
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            if (culturesListBox.SelectedIndex == -1)
            {
                MessageBox.Show("You should select the template culture first");
                return;
            }

            if (this.IsCultureExist(nameTextBox.Text))
            {
                MessageBox.Show(string.Format("Culture with the name \"{0}\" already exist. Please enter another name", nameTextBox.Text));
                return;
            }

            CultureListItem item = (CultureListItem)culturesListBox.SelectedItem;
            if (item == null)
            {
                return;
            }

            CultureInfo ci = new CultureInfo(item.CultureName);
            RegionInfo ri;

            ////Not every culture has a region info, that is why we must process the exception
            try
            {
                ri = new RegionInfo(ci.Name);
            }
            catch (Exception)
            {
                MessageBox.Show("This culture doesn't have the region information. Please select another culture");
                return;
            }

            CultureAndRegionInfoBuilder cib = null;
            try
            {
                cib = new CultureAndRegionInfoBuilder(nameTextBox.Text, CultureAndRegionModifiers.None);
            }
            catch (Exception)
            {
                MessageBox.Show(string.Format("The name \"{0}\" is invalid. Please enter another name", nameTextBox.Text));
                return;
            }

            cib.LoadDataFromCultureInfo(ci);
            cib.LoadDataFromRegionInfo(ri);
            cib.ConsoleFallbackUICulture = ci;
            cib.CultureEnglishName = enNameTextBox.Text;
            cib.CultureNativeName = nnTextBox.Text;
            cib.ThreeLetterISOLanguageName = isoNameTextBox.Text;
            cib.ThreeLetterWindowsLanguageName = winNameTextbox.Text;
            cib.TwoLetterISOLanguageName = iso2TextBox.Text;

            cib.RegionEnglishName = engNameNameTextBox.Text;
            cib.RegionNativeName = regNativeNameTextBox.Text;
            cib.ISOCurrencySymbol = isoCurrSymbolTextBox.Text;
            cib.CurrencyEnglishName = currencyEnglishNameTextBox.Text;
            cib.CurrencyNativeName = currNativeNameTextBox.Text;
            cib.NumberFormat.CurrencySymbol = curSymbolTextBox.Text;
            cib.Register();
            this.RefreshList();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            saveFileDialog1.ShowDialog();
        }

        private void openFileDialog1_FileOk(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var formatter = new BinaryFormatter();
            using (var stream = File.OpenRead(openFileDialog1.FileName))
                ExportList = (List<CultureExport>)formatter.Deserialize(stream);
            RefreshExportList();
        }

        private void saveFileDialog1_FileOk(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var formatter = new BinaryFormatter();
            using (var stream = File.Open(saveFileDialog1.FileName, FileMode.Create))
                formatter.Serialize(stream, ExportList);

        }

        private void button3_Click(object sender, EventArgs e)
        {
            AddToexport();
        }

        private void AddToexport()
        {
            if (culturesListBox.SelectedIndex == -1)
            {
                MessageBox.Show("You should select the template culture first");
                return;
            }

            CultureListItem item = (CultureListItem)culturesListBox.SelectedItem;
            if (item == null)
            {
                return;
            }

            CultureInfo ci = new CultureInfo(item.CultureName);
            if (ExportList.Any(c => c.cultureInfo.Name.Equals(ci.Name)))
                return;
            RegionInfo ri;

            ////Not every culture has a region info, that is why we must process the exception
            try
            {
                ri = new RegionInfo(ci.Name);
            }
            catch (Exception)
            {
                MessageBox.Show("This culture doesn't have the region information. Please select another culture");
                return;
            }
            CultureExport newExport = new CultureExport(ci, ri);

            ExportList.Add(newExport);
            RefreshExportList();
        }

        private void RefreshExportList()
        {
            ImportBox.Items.Clear();
            ExportList.Sort(new CultureExportCompere());


            foreach (var ci in ExportList)
            {
                if (!string.IsNullOrEmpty(ci.cultureInfo.Name))
                {
                    ci.imported = checkIfimported(ci);
                    CultureListItem item = new CultureListItem();
                    item.DisplayName = ci.cultureInfo.Name + " [" + ci.cultureInfo.EnglishName + "]";
                    item.CultureName = ci.cultureInfo.Name;
                    item.value = ci;
                    item.ItemColor = ci.imported ? Color.Green : Color.Red;
                    ImportBox.Items.Add(item);
                }
            }
        }

        private bool checkIfimported(CultureExport ci)
        {

            List<CultureInfo> culturesList = new List<CultureInfo>(CultureInfo.GetCultures(CultureTypes.AllCultures));

            foreach (var cl in culturesList)
            {
                if (cl.Name.Equals(ci.cultureInfo.Name))
                    return true;
            }
            return false;
        }

        private void ImportBox_DrawItem(object sender, DrawItemEventArgs e)
        {
            ListBox listBox = (ListBox)sender;
            if (e.Index == -1)
                return;
            var item = ImportBox.Items[e.Index] as CultureListItem; // Get the current item and cast it to MyListBoxItem
            if (item != null)
            {
                TextRenderer.DrawText(e.Graphics, item.ToString(), e.Font, new Point(e.Bounds.X, e.Bounds.Y), item.ItemColor);             
            }
            else
            {
                TextRenderer.DrawText(e.Graphics, item.ToString(), e.Font, new Point(e.Bounds.X, e.Bounds.Y), listBox.ForeColor);
                // The item isn't a MyListBoxItem, do something about it
            }
        }

        private void culturesListBox_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            int index = this.culturesListBox.IndexFromPoint(e.Location);
            if (index != System.Windows.Forms.ListBox.NoMatches)
            {
                AddToexport();
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (ImportBox.SelectedIndex == -1)
            {
                MessageBox.Show("You should select the template culture first to remove");
                return;
            }
            var listObject = ImportBox.SelectedItem as CultureListItem;
            if (listObject != null)
            {
                var expCulture = listObject.value as CultureExport;
                if (expCulture != null)
                {
                    for (int i = 0; i < ExportList.Count; i++)
                    {
                        if (ExportList[i] == expCulture)
                        {
                            ExportList.RemoveAt(i);
                            break;
                        }
                    }
                }
            }
            RefreshExportList();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (ImportBox.SelectedIndex == -1)
            {
                MessageBox.Show("You should select the template culture first to remove");
                return;
            }
            var listObject = ImportBox.SelectedItem as CultureListItem;
            if (listObject != null)
            {
                var expCulture = listObject.value as CultureExport;
                InmportCulture(expCulture);
            }
            RefreshExportList();
            RefreshList();
        }

        private void InmportCulture(CultureExport expCulture)
        {
            if (expCulture != null)
            {
                if (expCulture.imported)
                    return;
                CultureInfo ci = new CultureInfo(expCulture.cultureInfo.OrinincalCultureName);
                RegionInfo ri = new RegionInfo(expCulture.cultureInfo.OrinincalRegionTwoletter);

                ////Not every culture has a region info, that is why we must process the exception


                CultureAndRegionInfoBuilder cib = null;
                try
                {
                    cib = new CultureAndRegionInfoBuilder(expCulture.cultureInfo.Name, CultureAndRegionModifiers.None);
                }
                catch (Exception)
                {
                    MessageBox.Show(string.Format("The name \"{0}\" is invalid. Please enter another name", nameTextBox.Text));
                    return;
                }

                cib.LoadDataFromCultureInfo(ci);
                cib.LoadDataFromRegionInfo(ri);
                cib.ConsoleFallbackUICulture = ci;
                cib.CultureEnglishName = expCulture.cultureInfo.EnglishName;
                cib.CultureNativeName = expCulture.cultureInfo.NativeName;
                cib.ThreeLetterISOLanguageName = expCulture.cultureInfo.ThreeLetterISOLanguageName;
                cib.ThreeLetterWindowsLanguageName = expCulture.cultureInfo.ThreeLetterWindowsLanguageName;
                cib.TwoLetterISOLanguageName = expCulture.cultureInfo.TwoLetterISOLanguageName;
                cib.RegionEnglishName = expCulture.cultureInfo.RegionEnglishName;
                cib.RegionNativeName = expCulture.cultureInfo.RegionNativeName;
                cib.ISOCurrencySymbol = expCulture.cultureInfo.ISOCurrencySymbol;
                cib.CurrencyEnglishName = expCulture.cultureInfo.CurrencyEnglishName;
                cib.CurrencyNativeName = expCulture.cultureInfo.CurrencyNativeName;
                cib.NumberFormat.CurrencySymbol = expCulture.cultureInfo.CurrencySymbol;
                cib.Register();
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            foreach (var expCulture in ExportList)
            {
                InmportCulture(expCulture);
            }
            RefreshExportList();
            RefreshList();
        }

        private void ImportBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ImportBox.SelectedIndex == -1)
            {

                return;
            }
            var listObject = ImportBox.SelectedItem as CultureListItem;
            if (listObject != null)
            {
                var expCulture = listObject.value as CultureExport;
                if (expCulture != null)
                {
                    foreach (var it in culturesListBox.Items)
                    {
                        var item = it as CultureListItem;
                        if (item != null && item.CultureName.Equals(expCulture.cultureInfo.Name))
                        {
                            culturesListBox.SelectedItem = it;
                            break;
                        }
                    }
                }
            }

        }
    }
}
