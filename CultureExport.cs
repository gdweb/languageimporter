﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace LanguageRegistration
{
    [Serializable]
   public class CultureExport
    {     
        public bool imported { get; set; }
        public CultureExport()
        {
            cultureInfo = new CultureExportInfo();
        }
        public CultureExport(CultureInfo ci, RegionInfo ri)
        {
            cultureInfo = new CultureExportInfo
            {
                OrinincalCultureName = ci.GetConsoleFallbackUICulture().Name,
                FallbackCultureName = ci.GetConsoleFallbackUICulture().Name,
                Name = ci.Name,
                EnglishName = ci.EnglishName,
                NativeName = ci.NativeName,
                ThreeLetterISOLanguageName = ci.ThreeLetterISOLanguageName,
                ThreeLetterWindowsLanguageName = ci.ThreeLetterWindowsLanguageName,
                TwoLetterISOLanguageName = ci.TwoLetterISOLanguageName,
                RegionEnglishName = ri.EnglishName,
                RegionNativeName = ri.NativeName,
                ISOCurrencySymbol = ri.ISOCurrencySymbol,
                CurrencyEnglishName = ri.CurrencyEnglishName,
                CurrencyNativeName = ri.CurrencyNativeName,
                CurrencySymbol = ri.CurrencySymbol,
                RegionGeoid = ri.GeoId,
                OrinincalRegionTwoletter = ri.TwoLetterISORegionName
            };
            
        }

        public  CultureExportInfo cultureInfo { get; set; }   
    }
    [Serializable]
    public class CultureExportInfo
    {
        public string OrinincalCultureName { get; set; }
        public string OrinincalRegionTwoletter { get; set; }
        public string FallbackCultureName { get; set; }
        public string EnglishName { get; set; }
        public string NativeName { get; set; }
        public string Name { get; set; }
        public string ThreeLetterISOLanguageName { get; set; }
        public string ThreeLetterWindowsLanguageName { get; set; }
        public string TwoLetterISOLanguageName { get; set; }
        public string RegionEnglishName { get; set; }
        public string RegionNativeName { get; set; }
        public string ISOCurrencySymbol { get; set; }
        public string CurrencyEnglishName { get; set; }
        public string CurrencyNativeName { get; set; }
        public string CurrencySymbol { get; set; }
        public int RegionGeoid { get; set; }


    }
}
