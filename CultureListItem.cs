﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace LanguageRegistration
{
  class CultureListItem
  {
    public string DisplayName { get; set; }
    public string CultureName { get; set; }
    public Color ItemColor { get; set; }
    public object value { get; set; }

    public override string ToString() 
      {
        return DisplayName;
      }
  }
}
