﻿namespace LanguageRegistration
{
using System.Collections.Generic;
using System.Globalization;

  internal class CultureComparer : Comparer<CultureInfo>
  {
    // Methods
    public override int Compare(CultureInfo x, CultureInfo y)
    {
      return x.Name.CompareTo(y.Name);
    }
  }

    internal class CultureExportCompere: Comparer<CultureExport>
    {
        public override int Compare(CultureExport x, CultureExport y)
        {
            return x.cultureInfo.Name.CompareTo(y.cultureInfo.Name);
        }
    }
}
