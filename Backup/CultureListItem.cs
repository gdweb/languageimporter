﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LanguageRegistration
{
  class CultureListItem
  {
    public string DisplayName;
    public string CultureName;
    public override string ToString() 
      {
        return DisplayName;
      }
  }
}
