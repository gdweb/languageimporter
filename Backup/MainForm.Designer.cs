﻿namespace LanguageRegistration
{
  partial class MainForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }

      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.groupBox1 = new System.Windows.Forms.GroupBox();
      this.culturesListBox = new System.Windows.Forms.ListBox();
      this.groupBox2 = new System.Windows.Forms.GroupBox();
      this.groupBox6 = new System.Windows.Forms.GroupBox();
      this.label10 = new System.Windows.Forms.Label();
      this.btnUnRegister = new System.Windows.Forms.Button();
      this.label9 = new System.Windows.Forms.Label();
      this.btnRegister = new System.Windows.Forms.Button();
      this.groupBox5 = new System.Windows.Forms.GroupBox();
      this.LCIDtextBox = new System.Windows.Forms.TextBox();
      this.label8 = new System.Windows.Forms.Label();
      this.groupBox4 = new System.Windows.Forms.GroupBox();
      this.iso2TextBox = new System.Windows.Forms.TextBox();
      this.label7 = new System.Windows.Forms.Label();
      this.winNameTextbox = new System.Windows.Forms.TextBox();
      this.label5 = new System.Windows.Forms.Label();
      this.isoNameTextBox = new System.Windows.Forms.TextBox();
      this.label6 = new System.Windows.Forms.Label();
      this.groupBox3 = new System.Windows.Forms.GroupBox();
      this.ieftTextBox = new System.Windows.Forms.TextBox();
      this.label4 = new System.Windows.Forms.Label();
      this.nameTextBox = new System.Windows.Forms.TextBox();
      this.label3 = new System.Windows.Forms.Label();
      this.nnTextBox = new System.Windows.Forms.TextBox();
      this.label2 = new System.Windows.Forms.Label();
      this.enNameTextBox = new System.Windows.Forms.TextBox();
      this.label1 = new System.Windows.Forms.Label();
      this.groupBox7 = new System.Windows.Forms.GroupBox();
      this.groupBox9 = new System.Windows.Forms.GroupBox();
      this.curSymbolTextBox = new System.Windows.Forms.TextBox();
      this.label17 = new System.Windows.Forms.Label();
      this.isoCurrSymbolTextBox = new System.Windows.Forms.TextBox();
      this.label11 = new System.Windows.Forms.Label();
      this.currNativeNameTextBox = new System.Windows.Forms.TextBox();
      this.label15 = new System.Windows.Forms.Label();
      this.currencyEnglishNameTextBox = new System.Windows.Forms.TextBox();
      this.label16 = new System.Windows.Forms.Label();
      this.groupBox8 = new System.Windows.Forms.GroupBox();
      this.regNameTextBox = new System.Windows.Forms.TextBox();
      this.label12 = new System.Windows.Forms.Label();
      this.regNativeNameTextBox = new System.Windows.Forms.TextBox();
      this.label13 = new System.Windows.Forms.Label();
      this.engNameNameTextBox = new System.Windows.Forms.TextBox();
      this.label14 = new System.Windows.Forms.Label();
      this.groupBox1.SuspendLayout();
      this.groupBox2.SuspendLayout();
      this.groupBox6.SuspendLayout();
      this.groupBox5.SuspendLayout();
      this.groupBox4.SuspendLayout();
      this.groupBox3.SuspendLayout();
      this.groupBox7.SuspendLayout();
      this.groupBox9.SuspendLayout();
      this.groupBox8.SuspendLayout();
      this.SuspendLayout();
      // 
      // groupBox1
      // 
      this.groupBox1.Controls.Add(this.culturesListBox);
      this.groupBox1.Location = new System.Drawing.Point(12, 12);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new System.Drawing.Size(184, 506);
      this.groupBox1.TabIndex = 2;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "Defined languages";
      // 
      // culturesListBox
      // 
      this.culturesListBox.FormattingEnabled = true;
      this.culturesListBox.HorizontalScrollbar = true;
      this.culturesListBox.Location = new System.Drawing.Point(6, 14);
      this.culturesListBox.Name = "culturesListBox";
      this.culturesListBox.ScrollAlwaysVisible = true;
      this.culturesListBox.Size = new System.Drawing.Size(171, 485);
      this.culturesListBox.TabIndex = 1;
      this.culturesListBox.SelectedIndexChanged += new System.EventHandler(this.culturesListBox_SelectedIndexChanged);
      // 
      // groupBox2
      // 
      this.groupBox2.Controls.Add(this.groupBox6);
      this.groupBox2.Controls.Add(this.groupBox5);
      this.groupBox2.Controls.Add(this.groupBox4);
      this.groupBox2.Controls.Add(this.groupBox3);
      this.groupBox2.Location = new System.Drawing.Point(217, 12);
      this.groupBox2.Name = "groupBox2";
      this.groupBox2.Size = new System.Drawing.Size(469, 283);
      this.groupBox2.TabIndex = 3;
      this.groupBox2.TabStop = false;
      this.groupBox2.Text = "Culture settings";
      // 
      // groupBox6
      // 
      this.groupBox6.Controls.Add(this.label10);
      this.groupBox6.Controls.Add(this.btnUnRegister);
      this.groupBox6.Controls.Add(this.label9);
      this.groupBox6.Controls.Add(this.btnRegister);
      this.groupBox6.Location = new System.Drawing.Point(234, 200);
      this.groupBox6.Name = "groupBox6";
      this.groupBox6.Size = new System.Drawing.Size(228, 75);
      this.groupBox6.TabIndex = 11;
      this.groupBox6.TabStop = false;
      this.groupBox6.Text = "Register/Unregister";
      // 
      // label10
      // 
      this.label10.AutoSize = true;
      this.label10.Location = new System.Drawing.Point(6, 46);
      this.label10.Name = "label10";
      this.label10.Size = new System.Drawing.Size(90, 13);
      this.label10.TabIndex = 16;
      this.label10.Text = "Unregister culture";
      // 
      // btnUnRegister
      // 
      this.btnUnRegister.Location = new System.Drawing.Point(130, 46);
      this.btnUnRegister.Name = "btnUnRegister";
      this.btnUnRegister.Size = new System.Drawing.Size(75, 23);
      this.btnUnRegister.TabIndex = 15;
      this.btnUnRegister.Text = "Unregister";
      this.btnUnRegister.UseVisualStyleBackColor = true;
      this.btnUnRegister.Click += new System.EventHandler(this.btnUnRegister_Click);
      // 
      // label9
      // 
      this.label9.AutoSize = true;
      this.label9.Location = new System.Drawing.Point(6, 21);
      this.label9.Name = "label9";
      this.label9.Size = new System.Drawing.Size(104, 13);
      this.label9.TabIndex = 14;
      this.label9.Text = "Register new culture";
      // 
      // btnRegister
      // 
      this.btnRegister.Location = new System.Drawing.Point(130, 16);
      this.btnRegister.Name = "btnRegister";
      this.btnRegister.Size = new System.Drawing.Size(75, 23);
      this.btnRegister.TabIndex = 13;
      this.btnRegister.Text = "Register";
      this.btnRegister.UseVisualStyleBackColor = true;
      this.btnRegister.Click += new System.EventHandler(this.btnRegister_Click);
      // 
      // groupBox5
      // 
      this.groupBox5.Controls.Add(this.LCIDtextBox);
      this.groupBox5.Controls.Add(this.label8);
      this.groupBox5.Location = new System.Drawing.Point(6, 200);
      this.groupBox5.Name = "groupBox5";
      this.groupBox5.Size = new System.Drawing.Size(219, 75);
      this.groupBox5.TabIndex = 10;
      this.groupBox5.TabStop = false;
      this.groupBox5.Text = "Others";
      // 
      // LCIDtextBox
      // 
      this.LCIDtextBox.BackColor = System.Drawing.SystemColors.MenuBar;
      this.LCIDtextBox.Enabled = false;
      this.LCIDtextBox.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.LCIDtextBox.Location = new System.Drawing.Point(9, 32);
      this.LCIDtextBox.Name = "LCIDtextBox";
      this.LCIDtextBox.Size = new System.Drawing.Size(200, 22);
      this.LCIDtextBox.TabIndex = 13;
      // 
      // label8
      // 
      this.label8.AutoSize = true;
      this.label8.Location = new System.Drawing.Point(6, 16);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size(31, 13);
      this.label8.TabIndex = 12;
      this.label8.Text = "LCID";
      // 
      // groupBox4
      // 
      this.groupBox4.Controls.Add(this.iso2TextBox);
      this.groupBox4.Controls.Add(this.label7);
      this.groupBox4.Controls.Add(this.winNameTextbox);
      this.groupBox4.Controls.Add(this.label5);
      this.groupBox4.Controls.Add(this.isoNameTextBox);
      this.groupBox4.Controls.Add(this.label6);
      this.groupBox4.Location = new System.Drawing.Point(230, 19);
      this.groupBox4.Name = "groupBox4";
      this.groupBox4.Size = new System.Drawing.Size(233, 176);
      this.groupBox4.TabIndex = 9;
      this.groupBox4.TabStop = false;
      this.groupBox4.Text = "ISO";
      // 
      // iso2TextBox
      // 
      this.iso2TextBox.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.iso2TextBox.Location = new System.Drawing.Point(9, 110);
      this.iso2TextBox.Name = "iso2TextBox";
      this.iso2TextBox.Size = new System.Drawing.Size(200, 22);
      this.iso2TextBox.TabIndex = 21;
      this.iso2TextBox.Leave += new System.EventHandler(this.regNameTextBox_Leave);
      // 
      // label7
      // 
      this.label7.AutoSize = true;
      this.label7.Location = new System.Drawing.Point(6, 94);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(83, 13);
      this.label7.TabIndex = 20;
      this.label7.Text = "Two letter name";
      // 
      // winNameTextbox
      // 
      this.winNameTextbox.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.winNameTextbox.Location = new System.Drawing.Point(9, 71);
      this.winNameTextbox.Name = "winNameTextbox";
      this.winNameTextbox.Size = new System.Drawing.Size(200, 22);
      this.winNameTextbox.TabIndex = 19;
      this.winNameTextbox.Leave += new System.EventHandler(this.regNameTextBox_Leave);
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Location = new System.Drawing.Point(6, 55);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(137, 13);
      this.label5.TabIndex = 18;
      this.label5.Text = "Three letter Windows name";
      // 
      // isoNameTextBox
      // 
      this.isoNameTextBox.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.isoNameTextBox.Location = new System.Drawing.Point(9, 32);
      this.isoNameTextBox.Name = "isoNameTextBox";
      this.isoNameTextBox.Size = new System.Drawing.Size(200, 22);
      this.isoNameTextBox.TabIndex = 17;
      this.isoNameTextBox.Leave += new System.EventHandler(this.regNameTextBox_Leave);
      // 
      // label6
      // 
      this.label6.AutoSize = true;
      this.label6.Location = new System.Drawing.Point(6, 16);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(137, 13);
      this.label6.TabIndex = 16;
      this.label6.Text = "Three letter language name";
      // 
      // groupBox3
      // 
      this.groupBox3.Controls.Add(this.ieftTextBox);
      this.groupBox3.Controls.Add(this.label4);
      this.groupBox3.Controls.Add(this.nameTextBox);
      this.groupBox3.Controls.Add(this.label3);
      this.groupBox3.Controls.Add(this.nnTextBox);
      this.groupBox3.Controls.Add(this.label2);
      this.groupBox3.Controls.Add(this.enNameTextBox);
      this.groupBox3.Controls.Add(this.label1);
      this.groupBox3.Location = new System.Drawing.Point(6, 19);
      this.groupBox3.Name = "groupBox3";
      this.groupBox3.Size = new System.Drawing.Size(219, 176);
      this.groupBox3.TabIndex = 8;
      this.groupBox3.TabStop = false;
      this.groupBox3.Text = "Naming";
      // 
      // ieftTextBox
      // 
      this.ieftTextBox.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.ieftTextBox.Location = new System.Drawing.Point(9, 71);
      this.ieftTextBox.Name = "ieftTextBox";
      this.ieftTextBox.Size = new System.Drawing.Size(200, 22);
      this.ieftTextBox.TabIndex = 15;
      this.ieftTextBox.Leave += new System.EventHandler(this.regNameTextBox_Leave);
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(6, 55);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(64, 13);
      this.label4.TabIndex = 14;
      this.label4.Text = "ieftLangTag";
      // 
      // nameTextBox
      // 
      this.nameTextBox.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.nameTextBox.Location = new System.Drawing.Point(9, 32);
      this.nameTextBox.Name = "nameTextBox";
      this.nameTextBox.Size = new System.Drawing.Size(200, 22);
      this.nameTextBox.TabIndex = 13;
      this.nameTextBox.Leave += new System.EventHandler(this.regNameTextBox_Leave);
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(6, 16);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(35, 13);
      this.label3.TabIndex = 12;
      this.label3.Text = "Name";
      // 
      // nnTextBox
      // 
      this.nnTextBox.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.nnTextBox.Location = new System.Drawing.Point(9, 149);
      this.nnTextBox.Name = "nnTextBox";
      this.nnTextBox.Size = new System.Drawing.Size(200, 22);
      this.nnTextBox.TabIndex = 11;
      this.nnTextBox.Leave += new System.EventHandler(this.regNameTextBox_Leave);
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(6, 133);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(66, 13);
      this.label2.TabIndex = 10;
      this.label2.Text = "NativeName";
      // 
      // enNameTextBox
      // 
      this.enNameTextBox.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.enNameTextBox.Location = new System.Drawing.Point(9, 110);
      this.enNameTextBox.Name = "enNameTextBox";
      this.enNameTextBox.Size = new System.Drawing.Size(200, 22);
      this.enNameTextBox.TabIndex = 9;
      this.enNameTextBox.Leave += new System.EventHandler(this.regNameTextBox_Leave);
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(6, 94);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(70, 13);
      this.label1.TabIndex = 8;
      this.label1.Text = "English name";
      // 
      // groupBox7
      // 
      this.groupBox7.Controls.Add(this.groupBox9);
      this.groupBox7.Controls.Add(this.groupBox8);
      this.groupBox7.Location = new System.Drawing.Point(217, 300);
      this.groupBox7.Name = "groupBox7";
      this.groupBox7.Size = new System.Drawing.Size(468, 218);
      this.groupBox7.TabIndex = 4;
      this.groupBox7.TabStop = false;
      this.groupBox7.Text = "RegionInfo";
      // 
      // groupBox9
      // 
      this.groupBox9.Controls.Add(this.curSymbolTextBox);
      this.groupBox9.Controls.Add(this.label17);
      this.groupBox9.Controls.Add(this.isoCurrSymbolTextBox);
      this.groupBox9.Controls.Add(this.label11);
      this.groupBox9.Controls.Add(this.currNativeNameTextBox);
      this.groupBox9.Controls.Add(this.label15);
      this.groupBox9.Controls.Add(this.currencyEnglishNameTextBox);
      this.groupBox9.Controls.Add(this.label16);
      this.groupBox9.Location = new System.Drawing.Point(234, 19);
      this.groupBox9.Name = "groupBox9";
      this.groupBox9.Size = new System.Drawing.Size(228, 192);
      this.groupBox9.TabIndex = 14;
      this.groupBox9.TabStop = false;
      this.groupBox9.Text = "Currency settings";
      // 
      // curSymbolTextBox
      // 
      this.curSymbolTextBox.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.curSymbolTextBox.Location = new System.Drawing.Point(9, 73);
      this.curSymbolTextBox.Name = "curSymbolTextBox";
      this.curSymbolTextBox.Size = new System.Drawing.Size(200, 22);
      this.curSymbolTextBox.TabIndex = 15;
      // 
      // label17
      // 
      this.label17.AutoSize = true;
      this.label17.Location = new System.Drawing.Point(6, 57);
      this.label17.Name = "label17";
      this.label17.Size = new System.Drawing.Size(84, 13);
      this.label17.TabIndex = 14;
      this.label17.Text = "Currency symbol";
      // 
      // isoCurrSymbolTextBox
      // 
      this.isoCurrSymbolTextBox.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.isoCurrSymbolTextBox.Location = new System.Drawing.Point(9, 32);
      this.isoCurrSymbolTextBox.Name = "isoCurrSymbolTextBox";
      this.isoCurrSymbolTextBox.Size = new System.Drawing.Size(200, 22);
      this.isoCurrSymbolTextBox.TabIndex = 13;
      this.isoCurrSymbolTextBox.Leave += new System.EventHandler(this.regNameTextBox_Leave);
      // 
      // label11
      // 
      this.label11.AutoSize = true;
      this.label11.Location = new System.Drawing.Point(6, 16);
      this.label11.Name = "label11";
      this.label11.Size = new System.Drawing.Size(104, 13);
      this.label11.TabIndex = 12;
      this.label11.Text = "ISO currency symbol";
      // 
      // currNativeNameTextBox
      // 
      this.currNativeNameTextBox.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.currNativeNameTextBox.Location = new System.Drawing.Point(9, 151);
      this.currNativeNameTextBox.Name = "currNativeNameTextBox";
      this.currNativeNameTextBox.Size = new System.Drawing.Size(200, 22);
      this.currNativeNameTextBox.TabIndex = 11;
      this.currNativeNameTextBox.Leave += new System.EventHandler(this.regNameTextBox_Leave);
      // 
      // label15
      // 
      this.label15.AutoSize = true;
      this.label15.Location = new System.Drawing.Point(6, 135);
      this.label15.Name = "label15";
      this.label15.Size = new System.Drawing.Size(66, 13);
      this.label15.TabIndex = 10;
      this.label15.Text = "NativeName";
      // 
      // currencyEnglishNameTextBox
      // 
      this.currencyEnglishNameTextBox.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.currencyEnglishNameTextBox.Location = new System.Drawing.Point(9, 112);
      this.currencyEnglishNameTextBox.Name = "currencyEnglishNameTextBox";
      this.currencyEnglishNameTextBox.Size = new System.Drawing.Size(200, 22);
      this.currencyEnglishNameTextBox.TabIndex = 9;
      this.currencyEnglishNameTextBox.Leave += new System.EventHandler(this.regNameTextBox_Leave);
      // 
      // label16
      // 
      this.label16.AutoSize = true;
      this.label16.Location = new System.Drawing.Point(6, 96);
      this.label16.Name = "label16";
      this.label16.Size = new System.Drawing.Size(114, 13);
      this.label16.TabIndex = 8;
      this.label16.Text = "Currency english name";
      this.label16.Leave += new System.EventHandler(this.regNameTextBox_Leave);
      // 
      // groupBox8
      // 
      this.groupBox8.Controls.Add(this.regNameTextBox);
      this.groupBox8.Controls.Add(this.label12);
      this.groupBox8.Controls.Add(this.regNativeNameTextBox);
      this.groupBox8.Controls.Add(this.label13);
      this.groupBox8.Controls.Add(this.engNameNameTextBox);
      this.groupBox8.Controls.Add(this.label14);
      this.groupBox8.Location = new System.Drawing.Point(6, 19);
      this.groupBox8.Name = "groupBox8";
      this.groupBox8.Size = new System.Drawing.Size(219, 142);
      this.groupBox8.TabIndex = 9;
      this.groupBox8.TabStop = false;
      this.groupBox8.Text = "Naming";
      // 
      // regNameTextBox
      // 
      this.regNameTextBox.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.regNameTextBox.Location = new System.Drawing.Point(9, 32);
      this.regNameTextBox.Name = "regNameTextBox";
      this.regNameTextBox.Size = new System.Drawing.Size(200, 22);
      this.regNameTextBox.TabIndex = 13;
      this.regNameTextBox.Leave += new System.EventHandler(this.regNameTextBox_Leave);
      // 
      // label12
      // 
      this.label12.AutoSize = true;
      this.label12.Location = new System.Drawing.Point(6, 16);
      this.label12.Name = "label12";
      this.label12.Size = new System.Drawing.Size(35, 13);
      this.label12.TabIndex = 12;
      this.label12.Text = "Name";
      // 
      // regNativeNameTextBox
      // 
      this.regNativeNameTextBox.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.regNativeNameTextBox.Location = new System.Drawing.Point(9, 112);
      this.regNativeNameTextBox.Name = "regNativeNameTextBox";
      this.regNativeNameTextBox.Size = new System.Drawing.Size(200, 22);
      this.regNativeNameTextBox.TabIndex = 11;
      this.regNativeNameTextBox.Leave += new System.EventHandler(this.regNameTextBox_Leave);
      // 
      // label13
      // 
      this.label13.AutoSize = true;
      this.label13.Location = new System.Drawing.Point(6, 96);
      this.label13.Name = "label13";
      this.label13.Size = new System.Drawing.Size(66, 13);
      this.label13.TabIndex = 10;
      this.label13.Text = "NativeName";
      // 
      // engNameNameTextBox
      // 
      this.engNameNameTextBox.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.engNameNameTextBox.Location = new System.Drawing.Point(9, 73);
      this.engNameNameTextBox.Name = "engNameNameTextBox";
      this.engNameNameTextBox.Size = new System.Drawing.Size(200, 22);
      this.engNameNameTextBox.TabIndex = 9;
      this.engNameNameTextBox.Leave += new System.EventHandler(this.regNameTextBox_Leave);
      // 
      // label14
      // 
      this.label14.AutoSize = true;
      this.label14.Location = new System.Drawing.Point(6, 57);
      this.label14.Name = "label14";
      this.label14.Size = new System.Drawing.Size(70, 13);
      this.label14.TabIndex = 8;
      this.label14.Text = "English name";
      // 
      // Form1
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(698, 525);
      this.Controls.Add(this.groupBox7);
      this.Controls.Add(this.groupBox2);
      this.Controls.Add(this.groupBox1);
      this.MaximizeBox = false;
      this.Name = "Form1";
      this.Text = "Language registration";
      this.Load += new System.EventHandler(this.MainForm_Load);
      this.groupBox1.ResumeLayout(false);
      this.groupBox2.ResumeLayout(false);
      this.groupBox6.ResumeLayout(false);
      this.groupBox6.PerformLayout();
      this.groupBox5.ResumeLayout(false);
      this.groupBox5.PerformLayout();
      this.groupBox4.ResumeLayout(false);
      this.groupBox4.PerformLayout();
      this.groupBox3.ResumeLayout(false);
      this.groupBox3.PerformLayout();
      this.groupBox7.ResumeLayout(false);
      this.groupBox9.ResumeLayout(false);
      this.groupBox9.PerformLayout();
      this.groupBox8.ResumeLayout(false);
      this.groupBox8.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.ListBox culturesListBox;
    private System.Windows.Forms.GroupBox groupBox2;
    private System.Windows.Forms.GroupBox groupBox3;
    private System.Windows.Forms.TextBox ieftTextBox;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.TextBox nameTextBox;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.TextBox nnTextBox;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.TextBox enNameTextBox;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.GroupBox groupBox4;
    private System.Windows.Forms.TextBox winNameTextbox;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.TextBox isoNameTextBox;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.TextBox iso2TextBox;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.GroupBox groupBox5;
    private System.Windows.Forms.TextBox LCIDtextBox;
    private System.Windows.Forms.Label label8;
    private System.Windows.Forms.GroupBox groupBox6;
    private System.Windows.Forms.Label label10;
    private System.Windows.Forms.Button btnUnRegister;
    private System.Windows.Forms.Label label9;
    private System.Windows.Forms.Button btnRegister;
    private System.Windows.Forms.GroupBox groupBox7;
    private System.Windows.Forms.GroupBox groupBox8;
    private System.Windows.Forms.TextBox regNameTextBox;
    private System.Windows.Forms.Label label12;
    private System.Windows.Forms.TextBox regNativeNameTextBox;
    private System.Windows.Forms.Label label13;
    private System.Windows.Forms.TextBox engNameNameTextBox;
    private System.Windows.Forms.Label label14;
    private System.Windows.Forms.GroupBox groupBox9;
    private System.Windows.Forms.TextBox isoCurrSymbolTextBox;
    private System.Windows.Forms.Label label11;
    private System.Windows.Forms.TextBox currNativeNameTextBox;
    private System.Windows.Forms.Label label15;
    private System.Windows.Forms.TextBox currencyEnglishNameTextBox;
    private System.Windows.Forms.Label label16;
    private System.Windows.Forms.TextBox curSymbolTextBox;
    private System.Windows.Forms.Label label17;
  }
}

