﻿namespace LanguageRegistration
{
using System.Collections.Generic;
using System.Globalization;

  internal class CultureComparer : Comparer<CultureInfo>
  {
    // Methods
    public override int Compare(CultureInfo x, CultureInfo y)
    {
      return x.Name.CompareTo(y.Name);
    }
  }
}
