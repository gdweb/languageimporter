﻿namespace LanguageRegistration
{
  using System;
  using System.Collections.Generic;
  using System.Globalization;
  using System.Windows.Forms;

  public partial class MainForm : Form
  {
    public MainForm()
    {
      this.InitializeComponent();
    }

    private void MainForm_Load(object sender, EventArgs e)
    {
      this.RefreshList();     
    }
    
    private void RefreshList()
    {
      this.culturesListBox.Items.Clear();
      List<CultureInfo> culturesList = new List<CultureInfo>(CultureInfo.GetCultures(CultureTypes.AllCultures));
      culturesList.Sort(new CultureComparer());
      foreach (CultureInfo ci in culturesList)
      {
        if (!string.IsNullOrEmpty(ci.Name))
        {
          CultureListItem item = new CultureListItem();
          item.DisplayName = ci.Name + " [" + ci.EnglishName + "]";
          item.CultureName = ci.Name;
          culturesListBox.Items.Add(item);
        }
      }
    }

    private void FillFormWithCultureInfo(CultureInfo ci)
    {
      nnTextBox.Text = ci.NativeName;
      enNameTextBox.Text = ci.EnglishName;
      nameTextBox.Text = ci.Name;
      ieftTextBox.Text = ci.IetfLanguageTag;
      isoNameTextBox.Text = ci.ThreeLetterISOLanguageName;
      winNameTextbox.Text = ci.ThreeLetterWindowsLanguageName;
      iso2TextBox.Text = ci.TwoLetterISOLanguageName;
      LCIDtextBox.Text = ci.LCID.ToString();
      try 
      {
        RegionInfo regInfo = new RegionInfo(ci.Name);
        regNameTextBox.Text = regInfo.Name;
        regNativeNameTextBox.Text = regInfo.NativeName;
        engNameNameTextBox.Text = regInfo.EnglishName;
        currencyEnglishNameTextBox.Text = regInfo.CurrencyEnglishName;
        isoCurrSymbolTextBox.Text = regInfo.ISOCurrencySymbol;
        currNativeNameTextBox.Text = regInfo.CurrencyNativeName;
        curSymbolTextBox.Text = regInfo.CurrencySymbol;
      }
      catch (Exception)
        {
          regNameTextBox.Text = string.Empty;
          regNativeNameTextBox.Text = string.Empty;
          engNameNameTextBox.Text = string.Empty;
          currencyEnglishNameTextBox.Text = string.Empty;
          isoCurrSymbolTextBox.Text = string.Empty;
          currNativeNameTextBox.Text = string.Empty;
          curSymbolTextBox.Text = string.Empty;
        }
    }

    private void culturesListBox_SelectedIndexChanged(object sender, EventArgs e)
    {
      CultureListItem item = (CultureListItem)culturesListBox.SelectedItem;
      if (item == null)
      {
        return;
      }

      this.FillFormWithCultureInfo(new CultureInfo(item.CultureName));
    }

    private void btnUnRegister_Click(object sender, EventArgs e)
    {
      if (culturesListBox.SelectedIndex == -1)
      {
        MessageBox.Show("You should select a culture");
        return;
      }

      DialogResult result = MessageBox.Show("Are you sure you want to unregister selected culture?", "Confirmation", MessageBoxButtons.YesNoCancel);
      if (result == DialogResult.Yes)
      {
        CultureListItem item = (CultureListItem)culturesListBox.SelectedItem;
        CultureAndRegionInfoBuilder.Unregister(item.CultureName);
        this.RefreshList();
        item = (CultureListItem)culturesListBox.Items[0];
        this.FillFormWithCultureInfo(new CultureInfo(item.CultureName));
      }
    }

    private bool IsCultureExist(string cultureName)
    {
      foreach (CultureInfo ci in CultureInfo.GetCultures(CultureTypes.AllCultures))
      {
        if (ci.Name == cultureName)
        {
          return true;
        }
      }

      return false;
    }

    private void regNameTextBox_Leave(object sender, EventArgs e)
    {
      TextBox textbox = (TextBox)sender;
      if (string.IsNullOrEmpty(textbox.Text))
      {
        MessageBox.Show("You can't leave the textbox blank");
      }
    }

    private void btnRegister_Click(object sender, EventArgs e)
    {
      if (culturesListBox.SelectedIndex == -1)
      {
        MessageBox.Show("You should select the template culture first");
        return;
      }

      if (this.IsCultureExist(nameTextBox.Text))
      {
        MessageBox.Show(string.Format("Culture with the name \"{0}\" already exist. Please enter another name", nameTextBox.Text));
        return;
      }
      
      CultureListItem item = (CultureListItem)culturesListBox.SelectedItem;
      if (item == null)
      {
        return;
      }

      CultureInfo ci = new CultureInfo(item.CultureName);
      RegionInfo ri;
      
      ////Not every culture has a region info, that is why we must process the exception
      try
      {
        ri = new RegionInfo(ci.Name);
      }
      catch (Exception)
      {
        MessageBox.Show("This culture doesn't have the region information. Please select another culture");
        return;
      }

      CultureAndRegionInfoBuilder cib = null;
      try
      {
        cib = new CultureAndRegionInfoBuilder(nameTextBox.Text, CultureAndRegionModifiers.None);
      }
      catch (Exception)
      {
        MessageBox.Show(string.Format("The name \"{0}\" is invalid. Please enter another name", nameTextBox.Text));
        return;
      }

      cib.LoadDataFromCultureInfo(ci);
      cib.LoadDataFromRegionInfo(ri);
      cib.ConsoleFallbackUICulture = ci;
      cib.CultureEnglishName = enNameTextBox.Text;
      cib.CultureNativeName = nnTextBox.Text;
      cib.ThreeLetterISOLanguageName = isoNameTextBox.Text;
      cib.ThreeLetterWindowsLanguageName = winNameTextbox.Text;
      cib.TwoLetterISOLanguageName = iso2TextBox.Text;

      cib.RegionEnglishName = engNameNameTextBox.Text;
      cib.RegionNativeName = regNativeNameTextBox.Text;
      cib.ISOCurrencySymbol = isoCurrSymbolTextBox.Text;
      cib.CurrencyEnglishName = currencyEnglishNameTextBox.Text;
      cib.CurrencyNativeName = currNativeNameTextBox.Text;
      cib.NumberFormat.CurrencySymbol = curSymbolTextBox.Text;
      cib.Register();
      this.RefreshList();
    }
  }
}
